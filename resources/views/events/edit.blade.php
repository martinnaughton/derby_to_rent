@extends('layouts.head')
@include('layouts.header')
<div class="container">
<div class="row">
    <div class="col-6">
        <h1 class="display-3">Update an event</h1>
        <form method="post" action="{{ route('events.update', $event->id) }}">
            @method('PATCH') 
            @csrf
            <div class="form-group">

                <label for="title">Title:</label>
                <input type="text" class="form-control" name="title" value={{ $event->title }} />
            </div>

            <div class="form-group">
                <label for="image">Image:</label>
                <input type="text" class="form-control" name="image" value={{ $event->image }} />
            </div>

            <div class="form-group">
                <label for="type_of_event">Date start:</label>
                <input type="text" class="form-control" name="type_of_event" value={{ $event->start }} />
            </div>
            <div class="form-group">
                <label for="date">Date end:</label>
                <input type="date" class="form-control" name="date" value={{ $event->end }} />
            </div>
            <div class="form-group">
                <label for="description">description:</label>
                <input type="text" class="form-control" name="description" value={{ $event->description }} />
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
    <div class="col-6">
        <form action="{{route('event.destroy', $event->id)}}" method="post">
            @method('DELETE')
            @csrf
            <button class="btn btn-danger" type="submit">U can Delete too</button>
        </form>
    </div>
</div>
</div>
@include('layouts.footer')
