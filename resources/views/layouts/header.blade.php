
<header>
    <div id="nav-bar">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
        <a class="navbar-brand" href="/footballpitch">
        <img class="nav-logo" src="/assets/logo_transparent.png" class="d-inline-block align-top" alt="">
        </a>
        <div class="top-right links">
        <a class="nav-link" href="{{ url('/dashboard') }}">Login</a>
        @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                    @php
                    $id_user = Auth::user()->id;
                    @endphp
                        <a class="nav-link" href="/try">News</a>
                        <a class="nav-link" href="/footballpitch" >Home</a>
                        <a class="nav-link" href="{{ url('/home') }}">Dashboard</a>
                        <a class="nav-link" href="{{ url('/contacts/create') }}">Contact us</a>
                        <a class="nav-link" href="{{ url('/logout') }}">Logout</a>
                        <a class="nav-link" href="{{route('home.settings',$id_user)}}">Settings</a>
                        </div> 
                    @else
                        <a class="nav-link" href="{{ url('/dashboard') }}">Login</a>

                        @if (Route::has('register'))
                            <a class="nav-link" href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif
            @if(Auth::guest())
            
                                    @endif
                                    </div> 

        </div>
    </div>
    </nav>
    </header>
    <div class="container first-container">
    @yield('main')
    </div>
</div>