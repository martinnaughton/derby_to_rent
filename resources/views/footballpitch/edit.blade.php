<html>
@extends('layouts.head')

@include('layouts.header')
<body>
@section('main')
<div class="container first-container">
    <div class="row">
        <div class="col-6">
            <div class="card card-form-pitch">
            <h1>Edit football a pitch</h1>
            <form  method="GET"     action="{{route('footballpitch.update',$pitch->id)}}">
                @csrf

                <div class="form-group ">
                    <label for="name">Name:</label>
                    <input type="text" name="name" value="{{$pitch->name}}" />
                </div>

                <div class="form-group ">
                    <label for="image">image link1:</label>
                    <input type="text" name="image"  value="{{$pitch->image}}" />
                </div>

                <div class="form-group ">
                    <label for="image2">image2link:</label>
                    <input type="text" name="image2" value="{{$pitch->image2}}" />
                </div>
                <div class="form-group ">
                    <label for="address">address:</label>
                    <input type="text" name="address"  value="{{$pitch->address}}"/>
                </div>

                <div class="form-group ">
                    <label for="city">city:</label>
                    <input type="text" name="city" value="{{$pitch->city}}" />
                </div>
                <div class="form-group ">
                    <label for="description">description:</label>
                    <input type="text" name="description" value="{{$pitch->description}}" />
                </div>
                <div class="form-group ">
                    <label for="price_h">price_h:</label>
                    <input type="text" name="price_h"  value="{{$pitch->price_h}}"/>
                </div>
                <div class="form-group ">
                    <label for="max_player">max_player:</label>
                    <input type="text" name="max_player"  value="{{$pitch->max_player}}"/>
                </div>
                <div class="form-group ">
                    <label for="postalcode">postalcode:</label>
                    <input type="text" name="postalcode"  value="{{$pitch->postalcode}}"/>
                </div>
                 <div class="form-group ">
                    <label for="long">long:</label>
                    <input type="text" name="long" value="{{$pitch->long}}" />
                </div>
                 <div class="form-group ">
                    <label for="lat">lat:</label>
                    <input type="text" name="lat"  value="{{$pitch->lat}}"/>
                </div>
                <div class="form-group ">
                    <label for="landlord">landlord:</label>
                    <input type="text" name="landlord" value="{{$pitch->landlord}}" />
                </div>
                <div class="form-group ">
                    <label for="equipment">equipment:</label>
                    <input type="text" name="equipment" value="{{$pitch->equipment}}" />
                </div>
                <button class="btn btn-primary-btn" type="submit">Update</button>
            </form>
            </div>  
        </div>  
    </div>
</div>
@include('layouts.footer')
</body>
<html>