<html>
@extends('layouts.head')

@include('layouts.header')
<body>
@section('main')
<div class="container first-container">
    <div class="row">
        <div class="col-6">
            <div class="card card-form-pitch">
            <h1>Create football a pitch</h1>
            <form  method="post" action="{{ route('footballpitch.store') }}">
                @csrf

                <div class="form-group ">
                    <label for="name">Name:</label>
                    <input type="text" name="name" />
                </div>

                <div class="form-group ">
                    <label for="image">image link1:</label>
                    <input type="text" name="image" />
                </div>

                <div class="form-group ">
                    <label for="image2">image2link:</label>
                    <input type="text" name="image2" />
                </div>
                <div class="form-group ">
                    <label for="address">address:</label>
                    <input type="text" name="address" />
                </div>

                <div class="form-group ">
                    <label for="city">city:</label>
                    <input type="text" name="city" />
                </div>
                <div class="form-group ">
                    <label for="description">description:</label>
                    <input type="text" name="description" />
                </div>
                <div class="form-group ">
                    <label for="price_h">price_h:</label>
                    <input type="text" name="price_h" />
                </div>
                <div class="form-group ">
                    <label for="max_player">max_player:</label>
                    <input type="text" name="max_player" />
                </div>
                <div class="form-group ">
                    <label for="postalcode">postalcode:</label>
                    <input type="text" name="postalcode" />
                </div>
                 <div class="form-group ">
                    <label for="long">long:</label>
                    <input type="text" name="long" />
                </div>
                 <div class="form-group ">
                    <label for="lat">lat:</label>
                    <input type="text" name="lat" />
                </div>
                <div class="form-group ">
                    <input type="hidden" name="landlord" value="{{$id_user ?? ''}}" />
                </div>
                <div class="form-group ">
                    <label for="equipment">equipment:</label>
                    <input type="text" name="equipment" />
                </div>
                <button class="btn btn-primary-btn" type="submit">Create</button>
            </form>
          </div>  
        </div>  
    </div>
</div>
@include('layouts.footer')
</body>
<html>