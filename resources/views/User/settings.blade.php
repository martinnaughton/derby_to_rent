@extends('layouts.head')

<html>
@include('layouts.header')
    <body>
        <div class="container">
                <div class="row">
                    <div class="offset-2 col-8">
                        <div class="user-settings-card">
                            <div class="row">
                                <div class="col-4">
                                 <img src="{{$user->image}}"/>
                                </div>
                                <div class="col-8">
                                    <h4>Name:{{$user->name}}</h4>
                                    <h4>Surname:{{$user->surname}}</h4>
                                    <h4>Email:{{$user->email}}</h4>
                                    <h4>Number:{{$user->phone_number}}</h4>
                                    <h4>Password: ********* <small> <a href="{{ route('password.request') }}">(change password)</a></small></h4>
                                    

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </body>
@include('layouts.footer')
</html>
