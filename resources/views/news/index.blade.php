@extends('base')
@section('main')
<html>
    <head>
        <h1> Show News </h1>
        <a style="margin: 19px;" href="{{ route('news.create')}}" class="btn btn-primary">Return</a>

        <div>
    <a style="margin: 19px;" href="{{ route('news.create')}}" class="btn btn-primary">New ticket</a>
    </div>  
    </head>
    <div class="col-sm-12">
@section('main')
<div class="row">
<div class="col-sm-12">
    <h1 class="display-3">News</h1>    
  <table class="table table-striped">
    <thead>
        <tr>
          <td>Title</td>
          <td>Image</td>
          <td>Type of news</td>
          <td>Date</td>
          <td>Description</td>
          <td colspan = 2>Actions</td>
        </tr>
    </thead>  <tbody>
        @foreach($news as $new)
        <tr>
            <td>{{$new->title}}</td>
            <td>{{$new->image}}</td>
            <td>{{$new->type_of_news}}</td>
            <td>{{$new->date}}</td>
            <td>{{$new->description}}</td>
            <td>
                <a href="{{ route('news.edit',$new->id)}}" class="btn btn-primary">Edit</a>
            </td>
            <td>
                <form action="{{ route('news.destroy', $new->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>  
  </table>
<div>
</div>
@endsection


  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>
</html>
