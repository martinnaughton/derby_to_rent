import request from '@/utils/request';
const api = "/api/";

export function ticketList() {
    return request({
      url: 'ticket',
      method: 'get',
    });
  }
  export function TicketTop() {
    return request({
      url: 'ticket/ranking/top',
      method: 'get',
    });
  }
export function fetchticket(id) {
    return request({
      url: api + 'ticket' + id,
      method: 'get',
    });
  } 
  
export function createTicket(data) {
    return request({
      url: '/api/ticket/create',
      method: 'post',
      data,
    });
  }

export function acceptticket(id) {
    return request({
      url: 'ticket/accept/' + id,
      method: 'get',
    });
  }

export function rejectticket(id) {
    return request({
      url: 'ticket/reject/' + id,
      method: 'get',
    });
  } 
export function MyTicket(id) {
    return request({
      url: 'ticket/user/' + id,
      method: 'get',
    });
  } 
  export function PastTicket(id) {
    return request({
      url: 'ticket/user/past/' + id,
      method: 'get',
    });
  } 
  export function TicketBYUser(id) {
    return request({
      url: 'pitch/all/ticket/' + id,
      method: 'get',
    });
  } 