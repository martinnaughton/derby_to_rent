import Layout from '@/views/layout/Layout';

const landlordroute = {
    path: '/landlord',
    component: Layout,
    meta: { title: 'Landlord Amm.', icon: 'money', noCache: true },
    children: [
      {
        path: 'pitch',
        name: 'Mypitch',
        component: require('@/views/servio/landlord/mypitch').default,
        meta: { title: 'My pitches', icon: 'user' },
      },
      {
        path: 'ticket',
        name: 'Myticket',
        component: require('@/views/servio/landlord/myticket').default,
        meta: { title: 'My Ticket', icon: 'user' },
      },
      {
        path: 'editpitch/:id',
        name: 'editPitch',
        hidden: true,
        component: require('@/views/servio/index3').default,
        meta: { title:'editPitch' , hidden: true },
        params: true,
      },
      {
        path: 'pitch/add',
        name: 'AddPitch',
        hidden: true,
        component: require('@/views/servio/index2').default,
        meta: { title: 'AddPitch',  hidden: true },
      },
      {
        path: 'event/add',
        name: 'AddEvent',
        hidden: true,
        component: require('@/views/servio/AddEvent').default,
        meta: { title: 'AddEvent', hidden: true, },
      },
    ]
};

export default landlordroute;
