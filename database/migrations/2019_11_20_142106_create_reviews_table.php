<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->Increments('id');
            $table->timestamps();
            $table->integer('star')->max('5');
            $table->text('comment');
            $table->integer('ticket')->unique();
            $table->foreign('ticket')->references('id')->on('tickets')->onDelete('cascade');
            $table->integer('account');
            $table->foreign('account')->references('id')->on('users')->onDelete('cascade');
            $table->integer('footballpitch');
            $table->foreign('footballpitch')->references('id')->on('footballpitches')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reviews');
    }
}
