<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {   
        User::create([
            'name' => 'Admin',
            'email' => 'admin@test.com',
            'password' => Hash::make('admin'),
            'role' => 'admin'
        ]);
        User::create([
            'name' => 'Editor',
            'email' => 'editor@test.com',
            'password' => Hash::make('editor'),
            'role' => 'landlord'
        ]);
        User::create([
            'name' => 'User',
            'email' => 'user@test.com',
            'password' => Hash::make('user'),
            'role' => 'user'
        ]);

        $role2 = Role::create(['name' => 'isAdmin']);
        $permission2 = Permission::create(['name' => 'Admin']);
        $role = Role::create(['name' => 'isLandlord']);
        $permission = Permission::create(['name' => 'Create a pitch']);
        $role1 = Role::create(['name' => 'isUser']);
        $permission1 = Permission::create(['name' => 'Buy ticket']);
        $role2->givePermissionTo($permission2);
        $role2->givePermissionTo($permission);
        $user2 = User::create([
            'name'=>'admin',
            'surname'=>'admin',
            'email'=> 'your@email.com',
            'password' =>  bcrypt('12345678'),
            'phone_number' => '218232',
            'api_token' => Str::random(60),
            'role' => 'admin'
        ]);
        $user2->assignRole('isAdmin');
        $role->givePermissionTo($permission);
        $user = User::create([
            'name'=> "dio",
            'surname'=> "boia",
            'email' => 'dioboia@gmail.com',
            'password' => bcrypt('12345678'),
            'phone_number' => '218232',
            'api_token' => Str::random(60),
            'role' => 'landlord'
        ]);
        $user->assignRole('isLandlord');
        $role1->givePermissionTo($permission1);
        $users = User::create([
            'name'=> "marco",
            'surname'=> "verdi",
            'email' => 'marcoverdi@gmail.com',
            'password' => bcrypt('12345678'),
            'phone_number' => '3455642781',
            'api_token' => Str::random(60),
            'image'=> 'https://member.imagineacademy.microsoft.com/sites/all/themes/custom/ita_members/images/microsoft-img.png',
            'role' => 'user'
        ]);
        $users->assignRole('isUser');
        $user3 = User::create([
            'name'=> "alessio",
            'surname'=> "gialli",
            'email' => 'alessiogialli@gmail.com',
            'password' => bcrypt('12345678'),
            'phone_number' => '3937878543',
            'api_token' => Str::random(60),
            'image'=> 'https://member.imagineacademy.microsoft.com/sites/all/themes/custom/ita_members/images/microsoft-img.png',
        ]);
       $user3->assignRole('isUser');
        DB::table('users')->insert([
            'name'=>'Alex',
            'surname'=>'Pierre',
            'image'=>'https://jeaniuspublishing.com/wp-content/uploads/2016/02/HeadShot.jpg',
            'phone_number'=>'213892137',
            'email'=>'boia61@fedsnb.com',
            'api_token' => Str::random(60),
            'password' => bcrypt('12345678')
        ]);
        DB::table('equipment')->insert([
            'name' => 'In a military context, the term "materiel" refers either to the specific needs (excluding manpower) ',
            'description'=> 'Materiel management consists of continuing actions relating to planning, organizing, directing, coordinating, controlling, and evaluating the application of resources to ensure the effective and economical support of military forces. It includes provisioning, cataloging, requirements determination, acquisition, distribution, maintenance, and disposal. The terms "materiel management", "materiel control", "inventory control", "inventory management", and "supply management" are synonymous'
        ]);
        DB::table('statuses')->insert([
            'name'=> ' selection',
            'meaning' => ' The Selection is a dystopian/romance young adult novel by author Kiera Cass, following the journey of America Singer, a young girl who is entered in a competition called the Selection (referred to as The Hunger Games meets The Bachelor) to be the next queen.',
        ]);
        DB::table('statuses')->insert([
            'name'=> ' accepted',
            'meaning' => ' Accepted is a 2006 American comedy film directed by Steve Pink and written by Adam Cooper, Bill Collage and Mark Perez. The plot follows a group of high school graduates who create their own fake college after being rejected from the colleges to which they applied.',
        ]);
        DB::table('statuses')->insert([
            'name'=> ' rejected',
            'meaning' => ' Rejected is an animated short comedy film by Don Hertzfeldt that was released in 2000. It was nominated for an Academy Award for Best Animated Short Film the following year at the 73rd Academy Awards[1], and received 27 awards from film festivals around the world.',
        ]);
        DB::table('news')->insert([
            'title'=>'Title',
            'image'=>'http://www.like-agency.it/media/k2/items/cache/d6086de322f98f66cc694f32ea284557_XL.jpg',
            'url'=>'https://it.wikipedia.org/wiki/Salmo_(rapper)',
            'author' =>'Salmo',
            'date'=>'2000-01-01',
            'description'=>'Salmo ha iniziato la sua carriera musicale alletà di 13 anni, incidendo le sue prime rime tra il 1997 e il 1998.[2] Nel 1999 ha realizzato e pubblicato i demo Premeditazione e dolo con i rapper olbiesi Bigfoot e Scascio (con i quali formava il gruppo Premeditazione e Dolo), mentre nel 2004 ha pubblicato il primo demo da solista, intitolato Sotto pelle;[2] lanno successivo ha autoprodotto e pubblicato il secondo demo Mr. Antipatia.'
         ]);
        
         DB::table('footballpitches')->insert([
            'name'=> 'Anfield',
            'image'=> 'https://previews.123rf.com/images/coward_lion/coward_lion1808/coward_lion180800735/108016412-liverpool-united-kingdom-may-17-2018-anfield-stadium-the-home-ground-of-liverpool-fc-which-has-a-sea.jpg',
            'image2'=> 'https://ichef.bbci.co.uk/news/660/media/images/77766000/jpg/_77766716_lfc_view01_final.jpg',
            'city'=>'Liverpool',
            'address'=> 'Anfield Rd',
            'postalcode'=> 'L4 0TH',
            'landlord' => '2',
            'equipment' => '1',
            'long'=> '-2.960594',
            'max_player' => '22',
            'lat'=> '53.430701',
            'status' => '2',
            'price_h'=> '150',
            'description' => 'A football pitch (also known as a football field[1] or soccer field) is the playing surface for the game of association football. Its dimensions and markings are defined by Law 1 of the Laws of the Game, "The Field of Play".[2] The pitch is typically made of natural turf or artificial turf, although amateur and recreational teams often play on dirt fields. Artificial surfaces must be green in colour.'
            ]);
            DB::table('footballpitches')->insert([
                'name'=> 'Ormeau Park Playing Fields',
                'image' => 'https://discovernorthernireland.com/contentassets/c97254231f184a0a90d772bfb9398411/ormeau-park5/?height=310&width=545&mode=crop',
                'image2'=> 'https://discovernorthernireland.com/contentassets/c97254231f184a0a90d772bfb9398411/ormeau-park/?height=310&width=545&mode=crop',
                'city'=> 'Belfast',
                'address'=> 'Ormeau Park Ormeau Road',
                'postalcode'=> 'BT7 3GG',
                'landlord'=> '2',
                'equipment'=> '1',
                'status' => '2',
                'price_h'=> '25',
                'long'=> '-5.91641154853609',
                'lat'=> '54.590202243886',
                'max_player' => '22',
                'description' => 'A football pitch (also known as a football field[1] or soccer field) is the playing surface for the game of association football. Its dimensions and markings are defined by Law 1 of the Laws of the Game, "The Field of Play".[2] The pitch is typically made of natural turf or artificial turf, although amateur and recreational teams often play on dirt fields. Artificial surfaces must be green in colour.'
            ]);
            DB::table('footballpitches')->insert([
                'name'=> 'Hammer Playing Fields',
                'image' => 'http://www.haffeysportsgrounds.co.uk/wp-content/uploads/2013/06/IMG_0620-650x400.jpg',
                'image2'=> 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRxBsHuPY8cwBz5fskvBKxqCMfLpfllxw-DA5YHUcdmLEvOKhD9&s',
                'city'=> 'Belfast',
                'address'=> 'Agnes Street Shankill Road',
                'postalcode'=> 'BT13 1GG',
                'landlord'=> '2',
                'equipment'=> '1',
                'status' => '2',
                'max_player'=> '8',
                'price_h'=> '50',
                'long'=> '-5.94504066745723',
                'lat'=> '54.6061398477139',
                'description' => 'A football pitch (also known as a football field[1] or soccer field) is the playing surface for the game of association football. Its dimensions and markings are defined by Law 1 of the Laws of the Game, "The Field of Play".[2] The pitch is typically made of natural turf or artificial turf, although amateur and recreational teams often play on dirt fields. Artificial surfaces must be green in colour.'
            ]);
            DB::table('footballpitches')->insert([
                'name'=> 'Ulidia Playing Fields',
                'image' => 'https://pbs.twimg.com/media/D-QA7xyWsAUE5yQ.jpg',
                'image2'=> 'http://www.thenafl.co.uk/upload/grounds/1281447717Rosario%20pitch.jpg',
                'city'=> 'Belfast',
                'address'=> 'Ormeau Road',
                'postalcode'=> 'BT7 2GD',
                'landlord'=> '1',
                'status' => '2',
                'max_player'=> '10',
                'equipment'=> '1',
                'price_h'=> '34',
                'long'=> '-5.91554971565789',
                'lat'=> '54.577527808237',
                'description' => 'A football pitch (also known as a football field[1] or soccer field) is the playing surface for the game of association football. Its dimensions and markings are defined by Law 1 of the Laws of the Game, "The Field of Play".[2] The pitch is typically made of natural turf or artificial turf, although amateur and recreational teams often play on dirt fields. Artificial surfaces must be green in colour.'
            ]);
            DB::table('events')->insert([
                'title'=>'snow',
                'image'=>'https://i.ytimg.com/vi/7JyE47-Ykjo/maxresdefault.jpg',
                'start'=>'2019-11-30',
                'end'=>'2019-12-10',
                'footballpitch_id' => '1',
                'description'=>'Rain is liquid water in the form of droplets that have condensed from atmospheric water vapor and then become heavy enough to fall under gravity.'
             ]);
             DB::table('events')->insert([
                'title'=>'rain',
                'image'=>'https://i.ytimg.com/vi/7JyE47-Ykjo/maxresdefault.jpg',
                'start'=>'2019-11-27',
                'end'=>'2019-11-28',
                'footballpitch_id' => '1',
                'description'=>'Rain is liquid water in the form of droplets that have condensed from atmospheric water vapor and then become heavy enough to fall under gravity.'
             ]);
            //tickets
            
            DB::table('tickets')->insert([
                'date' => '22/01/2019',
                'time' => '11:30',
                'import'=> '40',
                'number' => '2',
                'footballpitch'  => '1',
                'account' => '4',
                'status'=> '2'
                ]);
            DB::table('tickets')->insert([
                'date' => '17/03/2020',
                'time' => '22:35',
                'import'=> '18',
                'number' => '1',
                'footballpitch'  => '2',
                'account' => '3',
                'status'=> '2'
                ]);
            DB::table('tickets')->insert([
                'date' => '24/12/2020',
                'time' => '00:30',
                'import'=> '18',
                'number' => '1',
                'footballpitch'  => '3',
                'account' => '2',
                'status'=> '2'
                ]);
            DB::table('tickets')->insert([
                'date' => '21/12/2019',
                'time' => '18:00',
                'import'=> '35',
                'number' => '4',
                'footballpitch'  => '1',
                'account' => '1',
                'status'=> '2'
                ]);
            DB::table('tickets')->insert([
                'date' => '08/08/2019',
                'time' => '16:15',
                'import'=> '55',
                'number' => '1',
                'footballpitch'  => '4',
                'account' => '3',
                'status'=> '2'
                ]);
            DB::table('tickets')->insert([
                'date' => '05/11/2020',
                'time' => '10:00',
                'import'=> '50',
                'number' => '2',
                'footballpitch'  => '2',
                'account' => '3',
                'status'=> '2'
                ]);
            DB::table('tickets')->insert([
                'date' => '27/01/2020',
                'time' => '12:30',
                'import'=> '30',
                'number' => '3',
                'footballpitch'  => '3',
                'account' => '2',
                'status'=> '2'
                ]);
            DB::table('tickets')->insert([
                'date' => '12/10/2019',
                'time' => '19:00',
                'import'=> '63',
                'number' => '2',
                'footballpitch'  => '4',
                'account' => '3',
                'status'=> '2'
                ]);
            DB::table('tickets')->insert([
                'date' => '19/07/2020',
                'time' => '16:30',
                'import'=> '30',
                'number' => '1',
                'footballpitch'  => '2',
                'account' => '3',
                'status'=> '2'
                ]);
            DB::table('tickets')->insert([
                'date' => '21/12/2020',
                'time' => '12:00',
                'import'=> '21',
                'number' => '3',
                'footballpitch'  => '4',
                'account' => '3',
                'status'=> '2'
                ]);
            DB::table('tickets')->insert([
                'date' => '09/04/2019',
                'time' => '14:15',
                'import'=> '45',
                'number' => '1',
                'footballpitch'  => '2',
                'account' => '4',
                'status'=> '2'
                ]);
            DB::table('tickets')->insert([
                'date' => '10/06/2019',
                'time' => '13:30',
                'import'=> '74',
                'number' => '2',
                'footballpitch'  => '3',
                'account' => '2',
                'status'=> '2'
                ]);
            DB::table('tickets')->insert([
                'date' => '11/09/2020',
                'time' => '15:45',
                'import'=> '20',
                'number' => '1',
                'footballpitch'  => '1',
                'account' => '2',
                'status'=> '2'
                ]);
            DB::table('tickets')->insert([
                'date' => '4/09/2019',
                'time' => '09:20',
                'import'=> '26',
                'number' => '2',
                'footballpitch'  => '4',
                'account' => '4',
                'status'=> '2'
                ]);
            DB::table('tickets')->insert([
                'date' => '16/07/2020',
                'time' => '11:00',
                'import'=> '27',
                'number' => '1',
                'footballpitch'  => '3',
                'account' => '2',
                'status'=> '2'
                ]);
            DB::table('tickets')->insert([
                'date' => '14/07/2019',
                'time' => '17:25',
                'import'=> '45',
                'number' => '1',
                'footballpitch'  => '2',
                'account' => '1',
                'status'=> '2'
                ]);
            DB::table('tickets')->insert([
                'date' => '27/02/2020',
                'time' => '19:30',
                'import'=> '50',
                'number' => '3',
                'footballpitch'  => '1',
                'account' => '3',
                'status'=> '2'
                ]);
            DB::table('tickets')->insert([
                'date' => '08/09/2019',
                'time' => '21:15',
                'import'=> '50',
                'number' => '2',
                'footballpitch'  => '4',
                'account' => '4',
                'status'=> '2'
                ]);
            DB::table('tickets')->insert([
                'date' => '12/11/2020',
                'time' => '14:05',
                'import'=> '40',
                'number' => '1',
                'footballpitch'  => '1',
                'account' => '1',
                'status'=> '2'
                ]);
            DB::table('tickets')->insert([
                'date' => '16/05/2019',
                'time' => '20:30',
                'import'=> '25',
                'number' => '2',
                'footballpitch'  => '3',
                'account' => '4',
                'status'=> '2'
                ]);
                 //reviewss
                DB::table('reviews')->insert([
                'comment'=> ' good',
                'ticket' => '1',
                'star' => ' 4',
                'account' => '3',
                'footballpitch' => '3',
                'created_at' => ' 2019-10-22 12:31:06'
            ]);
            DB::table('reviews')->insert([
                'comment'=> ' not bad',
                'star' => ' 5',
                'ticket' => '2',
                'account' => '3',
                'footballpitch' => '1',
                'created_at' => ' 2019-11-12 12:31:06'
            ]);
            DB::table('reviews')->insert([
                'comment'=> ' good',
                'star' => ' 4',
                'ticket' => '3',
                'account' => '3',
                'footballpitch' => '2'
                ,'created_at' => ' 2019-10-22 12:31:06'
            ]);
            DB::table('reviews')->insert([
                'comment'=> ' good',
                'star' => ' 5',
                'ticket' => '5',
                'account' => '3',
                'footballpitch' => '1',
                'created_at' => ' 2019-10-22 12:31:06'
            ]);
            
    }
}