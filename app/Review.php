<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $fillable = [
        'id',
        'comment',
        'ticket',
        'star',
        'account',
        'footballpitch',
        'created_at'
    ];
    public function ticket()
    {
        return $this->belongsTo('App\Ticket');
    }
}
