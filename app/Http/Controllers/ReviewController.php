<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\footballpitch;
use App\Equipment;
use App\Ticket;
use App\Review;
use App\User;
use Log;
use Illuminate\Support\Facades\DB;
use Auth;

class ReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createReview($id)
    {
        $userId = Auth::id();
        $users = User::all();
        $footballpitch = footballpitch::all();
        $user = User::find($userId);
        $pitch = DB::table('tickets')->where('id' , $id)->first();
        $reviews = DB::table('reviews')->where('footballpitch' , $pitch->footballpitch)->orderBy('created_at' , 'asc' )->get();
        if (Auth::check())
        {
            log::info('create');
            return view('review.create',compact('user','id','pitch','reviews','users','footballpitch'));
        }
        else
        {
            log::info('401');
        abort('401');
        }
        }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Log::info("Review Store");

        $review = new Review([
            'star' => $request->get('star'),
            'comment' => $request->get('comment'),
            'ticket' => $request->get('ticket'),
            'review' => $request->get('review'),
            'account' => $request->get('account'),
            'footballpitch' => $request->get('footballpitch')
            
        ]);
        Log::info($review);
        $review->save();
        return redirect('/home')->with('success', 'Review saved!');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        log::info('qui');
        $userId = Auth::id();
        $users = User::all();
        $footballpitch = footballpitch::all();
        $user = User::find($userId);
        $pitch = DB::table('tickets')->where('id' , $id)->first();
        $reviews = DB::table('reviews')
        ->join('tickets', 'reviews.ticket' ,'=','tickets.id')
        ->where('reviews.ticket', $id)
        ->get();
        $revi = Review::find($id);
        
        return view('review.edit', compact('reviews','footballpitch','user','pitch','users','id','revi'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Log::info($request);
        $request->validate([
            'star'=>'required',
            'account'=>'required',
            'footballpitch'=>'required'
        ]);
        Log::info($request);
        $review = Review::find($id);
        $review->star =  $request->get('star');
        $review->account = $request->get('account');
        $review->footballpitch = $request->get('footballpitch');
        $review->comment =  $request->get('comment');
        $review->ticket =  $request->get('ticket');
        $review->update();

        return redirect('/home')->with('success', 'Review updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Log::info("Review Delete");
        $review = Review::find($id);
        $review->delete();

        return redirect('/review')->with('success', 'Review deleted!');
    }
    public function delete($id)
    {
        $review = Review::find($id);
        $review->delete();

        return redirect('/home')->with('success', 'Review deleted!');
    }
}
