<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\footballpitch;
use App\Landlord;
use App\Equipment;
use App\Ticket;
use Log;
use App\User;
use Illuminate\Support\Facades\DB;

class TicketsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ticket = Ticket::all();
        return response()->json($ticket->toArray());
    }   

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'date'=>'required',
            'time'=>'required',
            'import'=>'required',
            'number'=>'required'
        ]);

        $ticket = new Ticket([
            'date' => $request->get('date'),
            'time' => $request->get('time'),
            'import' => $request->get('import'),
            'footballpitch' => $request->get('footballpitch'),
            'account' => $request->get('account'),
            
        ]);
        $ticket->number = $request->get('number');
        $ticket->save();
        return response()->json(['job' => 'success', 'ticket' => 'saved'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function show($id)
    {
       /* $ticket = Ticket::find($id);
        return  response()->json($ticket->toArray());*/
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ticket = Ticket::find($id);
        $ticket->date =  $request->get('date');
        $ticket->time = $request->get('time');
        $ticket->import = $request->get('import');
        $ticket->address = $request->get('address');
        $ticket->number = $request->get('number');
        $ticket->save();
        return response()->json(['job' => 'success', 'ticket' => 'updated'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ticket = Ticket::find($id);
        $ticket->delete();
        return response()->json(['job' => 'success', 'ticket' => 'deleted'], 200);
    }
    public function AcceptTicket($id)
    {
        $ticket = Ticket::find($id);
        if(   $ticket->status === 1 )
        {
            $ticket->status = 2 ; //accept
            $ticket->save();
        }
        return response()->json(['job' => 'success', 'ticket' => 'Accepted'], 200);
    }   
    public function RejectTicket($id)
    {
        $ticket = Ticket::find($id);
        if(   $ticket->status === 1 )
        {
            $ticket->status = 3 ; //reject
            $ticket->save();
        }
        return response()->json(['job' => 'success', 'ticket' => 'rejected'], 200);
    } 
    public function TopTicket()
    {   $array = [];
        $tickets = DB::table('tickets')->where( 'status' , '=' , 2 )->orderBy('import' , 'desc')->get();
        array_push( $array , $tickets);
        return response()->json($tickets);
    } 
    public function MyTicket($id)
    {
        $date = date('Y-m-d H:i:s', time());
        log::info($date);
        $ticket = DB::table('tickets')->where([[ 'account' , '=' , $id],['date' ,'>' ,$date],])->get();
        return response()->json($ticket);
    }
    public function MyReview($id)
    {
        $ticket = DB::table('reviews')->where( 'account' , '=' , $id)->get();
        return response()->json($ticket);
    }
    public function MyOldTicket($id)
    {
        $date = date('Y-m-d H:i:s', time());
        log::info($date);
        $ticket = DB::table('tickets')->where([[ 'account' , '=' , $id],['date' ,'<' ,$date],])->get();
        return response()->json($ticket);
    }
}
