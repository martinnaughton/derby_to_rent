<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\footballpitch;
use App\Event;
use Auth;

use Log;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Log::info("Event index");

        $events = Event::all();

        return view('events.index', compact('events'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $id_user = Auth::user()->id;
        $pitch = footballpitch::all()->where('landlord',$id_user);
        Log::info("Event create");

        return view('events.create', compact('pitch'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Log::info("Event store");


        $request->validate([
            'title'=>'required',
            'image'=>'required',
            'start'=>'required',
            'end'=>'required',
            'description'=>'required'
        ]); 

        $event = new Event([
            'title' => $request->get('title'),
            'image' => $request->get('image'),
            'start' => $request->get('start'),
            'end' => $request->get('end'),
            'description' => $request->get('description'),
            'footballpitch_id'  => $request->get('footballpitch_id')
        ]);
        $event->save();

        Log::info($event);
        return redirect('/events')->with('success', 'Event saved!');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        Log::info("Event edit");

        $event = Event::find($id);
        return view('events.edit', compact('event')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pitch = footballpitch::all();
        Log::info("Event update");

      /*  $request->validate([
            'title'=>'required',
            'image'=>'required',
            'type_of_event'=>'required',
            'date'=>'required',
            'description'=>'required'
        ]); */

        $event = Event::find($id);
        $event->title =  $request->get('title');
        $event->image = $request->get('image');
        $event->footballpitch_id = $request->get('footballpitch_id');
        $event->start = $request->get('start');
        $event->end = $request->get('end');
        $event->description = $request->get('description');
        $event->save();

        return redirect('/events')->with('success', 'Event updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Log::info("Event destroy");

        $event = Event::find($id);
        $event->delete();

        return redirect('/home')->with('success', 'Event deleted!');
    }
}
