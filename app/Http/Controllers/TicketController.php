<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\footballpitch;
use App\Landlord;
use App\Equipment;
use App\Ticket;
use App\User;
use Auth;
use Log;

class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $userId = Auth::id();
        $user = User::find($userId);
        if (Auth::check())
        {
        return view('ticket.create',compact('user'));
        }
        else
        {
        abort('401');
        }
        }

        public function createTicket($id)
        {
            Log::info($id);
            $pitch = footballpitch::find($id);
            $userId = Auth::id();
            $user = User::find($userId);
            $date = date("d/m/Y h:i:s");
            Log::info($user);
            if (Auth::check())
            {
            return view('ticket.create',compact('user','pitch','date'));
            }
            else
            {
            abort('401');
            }
            }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Log::info($request);
        
        $request->validate([
            'date'=>'required',
            'time'=>'required',
            'import'=>'required',
            'number'=>'required'
        ]);

        $ticket = new Ticket([
            'date' => $request->get('date'),
            'time' => $request->get('time'),
            'import' => $request->get('import'),
            'footballpitch' => $request->get('footballpitch'),
            'account' => $request->get('account'),
            
        ]);
        Log::info($ticket);
        $ticket->number = $request->get('number');
        $ticket->save();
        Log::info('ci so');
        return redirect('/home')->with('success', 'Ticket saved!');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (Auth::check())
        {
        Log::info("Show");

        $pitch = footballpitch::find($id);
        $landlord = Landlord::all();
        $equip = Equipment::all();
        $ticket = Ticket::all();

        return view('ticket.show', compact('pitch','landlord','equip','ticket'));}
        else
        {
        abort('401');
        }
        }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Log::info("Ticket Update");
        /*$request->validate([
            'date'=>'required',
            'time'=>'required',
            'import'=>'required',
            'address'=>'required',
            'number'=>'required'
        ]);*/

        $ticket = Ticket::find($id);
        $ticket->date =  $request->get('date');
        $ticket->time = $request->get('time');
        $ticket->import = $request->get('import');
        $ticket->address = $request->get('address');
        $ticket->number = $request->get('number');
        $ticket->save();

        return redirect('/ticket')->with('success', 'Ticket updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Auth::check())
        {

        Log::info("Ticket Delete");
        $ticket = Ticket::find($id);
        $ticket->delete();

        return redirect('/ticket')->with('success', 'Ticket deleted!');
        }
        else
            {
            abort('401');
            }
            }
}
