<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class footballpitch extends Model
{
    protected $fillable = [
        'id',
        'name',
        'image',
        'image2',
        'city',
        'address',
        'landlord',
        'equipment',
        'postalcode',
        'long',
        'lat',
        'desciption',
        'price_h',
        'max_player'
    ];
    public function event()
    {
        return $this->hasOne('App\Event');
    }
}
