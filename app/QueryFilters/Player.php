<?php


namespace App\QueryFilters;

class Player extends Filter
{
    protected function applyFilter($builder)
    {
        return $builder->where('max_player', '<=', request($this->filterName())) ;
    }
}