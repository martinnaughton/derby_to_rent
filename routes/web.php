<?php
use App\User;
use Barryvdh\Debugbar;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//debugbar example here
Route::get('/ciao', function () {
    \Debugbar::startMeasure('query_time' ,'The execution time of user query');
    $test = User::get();
    return view('welcome');
    \Debugbar::stopMeasure('query_time');
});
//fullcalender
Route::get('fullcalendar','FullCalendarController@index');
Route::post('fullcalendar/create','FullCalendarController@create');
Route::post('fullcalendar/update','FullCalendarController@update');
Route::post('fullcalendar/delete','FullCalendarController@destroy');

Route::get('/welcome', function () {
    return view('Item.calendar');
});
Route::get('/', function () {
    return redirect('/footballpitch');
    
});
Route::apiResource('/api/pitch', 'Api\PitchController');
Route::apiResource('/api/event', 'Api\EventsController');
Route::apiResource('/api/ticket', 'Api\TicketsController');
Route::apiResource('/api/dashboard', 'Api\DashboardController');
Route::apiResource('/api/review', 'Api\ReviewsController');
Route::get('/api/dashboard/ticket/year', 'Api\DashboardController@Tyear');
Route::get('/api/dashboard/ticket/year/money', 'Api\DashboardController@Pyear');
Route::get('/api/event/calendar/days', 'Api\EventsController@Calendar');
Route::get('/api/ticket/accept/{id}', 'Api\TicketsController@AcceptTicket');
Route::get('/api/ticket/reject/{id}', 'Api\TicketsController@RejectTicket');
Route::get('/api/ticket/ranking/top', 'Api\TicketsController@TopTicket');
Route::get('/api/ticket/user/{id}', 'Api\TicketsController@MyTicket');
Route::get('/api/ticket/user/past/{id}', 'Api\TicketsController@MyOldTicket');
Route::get('/api/pitch/all/ticket/{id}', 'Api\PitchController@TicketBYPitch');
Route::get('/api/review/user/{id}', 'Api\TicketsController@MyReview');
Route::get('/api/pitch/accept/{id}', 'Api\PitchController@AcceptPitch');
Route::get('/api/pitch/reject/{id}', 'Api\PitchController@RejectPitch');
Route::get('/api/pitch/landlord/{id}', 'Api\PitchController@Mypitch');
Route::get('/api/pitch/ranking/top10', 'Api\PitchController@top10ranking');
Route::get('/api/user/ranking/top', 'Api\UserController@TopUser');
//Route::get('/api/pitch/ranking/top20', 'Api\PitchController@ShowOrder');
Route::resource('/review' , 'ReviewController');
Route::resource('/footballpitch' , 'FootballpitchController');
Route::get('/logout' , 'Auth\LoginController@logout')->name('logout');
    Route::get('/review/delete/{id}' , 'ReviewController@delete')->name('review.delete');
    Route::get('/review/update/{id}' , 'ReviewController@update')->name('review.update');
    Route::get('/review/create/{id}' , 'ReviewController@createReview')->name('review.createreview');
    Route::get('/review/edit/{id}' , 'ReviewController@edit')->name('review.edit');
    Route::get('/review/delete/{id}' , 'ReviewController@delete')->name('review.delete');
    Route::resource('/ticket' , 'TicketController');
    Route::resource('/home/event' , 'EventController');
    Route::get('home/settings{id}' ,'HomeController@Setting')->name('home.settings');
    Route::get('/home/acc{id}' , 'HomeController@AcceptTicket')->name('home.accept');
    Route::get('/home/rej{id}' , 'HomeController@RejectTicket')->name('home.reject');
    Route::get('/ticket/create/{id}' , 'TicketController@createTicket')->name('ticket.createticket');
    Route::get('/home', 'HomeController@index')->name('home');
   // Auth::routes();
   Route::post('/login', '\Tuandm\Laravue\Http\Controllers\AuthController@login');
   Route::get('/login', '\Tuandm\Laravue\Http\Controllers\AuthController@laravue');
   //Route::post('/admin/pitch', '\Tuandm\Laravue\Http\Controllers\AuthController@laravue');
   Route::get('/login', function () {
    return redirect('/dashboard');
});
/*Route::group(['middleware' => 'auth'], function () {
    Route::get('/dashboard' , '\Tuandm\Laravue\Http\Controllers\LaravueController@index');
});*/
//Route::resource('/footballpitch/create/new' , 'AccountController@store');
//Route::resource('/account' , 'AccountController');
/*Route::resource('/ticket' , 'TicketController'){
    $ticket = Ticket::orderBy('date', 'asc')->get();
    return view('task', [
        'task'=> $ticket
    ]);
}*/

//Route::resource('/ticket' , 'TicketController');

//Route::get('/home', 'HomeController@index')->name('home');
//Route::get('/logout' , 'Auth\LoginController@logout')->name('logout');

//Route::resource('/user' , 'UserController');
//Route::resource('/role' , 'RoleController');

/*Route::get('/user', function () {
    return redirect('/setting');
});*/

//Route::resource('/setting' , 'AccountController');
Route::resource('/news', 'NewsController');

Route::resource('/events', 'EventController');

Route::resource('/contacts', 'ContactController');